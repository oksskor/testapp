package com.oxana.testapp;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity {


    private GetTimeTask mTask = null;

    private LinearLayout mView;
    private View mProgressView;
    private ListView mListView;
    private List<String> mData;
    private ArrayAdapter<String> mAdapter;
    private DbHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        db = new DbHelper(this);
        updateData();
        mView = (LinearLayout) findViewById(R.id.main);
        Button mChangeColorButton = (Button) findViewById(R.id.button_change_back_color);
        mChangeColorButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                changeBackColor();
            }
        });

        Button mDownloadDataButton = (Button) findViewById(R.id.button_get_data);
        mDownloadDataButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadData();
            }
        });

        mListView = (ListView) findViewById(R.id.list);
        mProgressView = findViewById(R.id.progress);

        mAdapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, mData);

        mListView.setAdapter(mAdapter);
    }


    private void changeBackColor() {
        mView.setBackgroundColor(Color.rgb((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255)));
    }

    private void downloadData() {
        if (mTask != null) {
            return;
        }
        showProgress(true);
        mTask = new GetTimeTask();
        mTask.execute((Void) null);
    }

    /**
     * Shows the progress UI and hides list
     */
    private void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mListView.setVisibility(show ? View.GONE : View.VISIBLE);
    }


    private void downloadUrl(String myurl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            is = conn.getInputStream();

            String date = parseIS(is);
            db.addItem(date);

        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String parseIS(InputStream is) throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line);
        }
        String tag = "[REQUEST_TIME] => ";
        int start = total.lastIndexOf(tag) + tag.length();
        int end = total.lastIndexOf(")");
        String date = total.substring(start, end);
        return date;

    }

    private String parseDate(String date) {
        int unixSeconds = Integer.valueOf(date);
        Date newDate = new Date(unixSeconds * 1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss"); // the format of your date
        sdf.setTimeZone(TimeZone.getDefault()); // give a timezone reference for formating (see comment at the bottom
        String str = sdf.format(newDate);
        return str;

    }

    private void updateData() {
        if (mData != null) {
            mData.clear();
        } else {
            mData = new ArrayList();
        }
        List<String> list  = db.getAllItems();

        for (String str : list) {
            String date = parseDate(str);
            mData.add(date);
        }
    }

    public class GetTimeTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {


            try {
                downloadUrl("http://android-logs.uran.in.ua/test.php");
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mTask = null;
            showProgress(false);

            if ((success)) {
                updateData();
                mAdapter.notifyDataSetChanged();
            } else {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Проверьте подключение к интернету и попробуйте еще раз", Toast.LENGTH_SHORT);
                toast.show();
            }
        }

        @Override
        protected void onCancelled() {
            mTask = null;
            showProgress(false);
        }
    }
}

